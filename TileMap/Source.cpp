#include <fstream>
#include "XLibrary11.hpp"
using namespace XLibrary11;

bool isHit(Float3 a, Float3 b, Float2 size)
{
	if (a.x - size.x < b.x &&
		a.x + size.x > b.x &&
		a.y - size.y < b.y &&
		a.y + size.y > b.y)
	{
		return true;
	}

	return false;
}

int Main()
{
	Camera camera;
	camera.color = Float4(0.5f, 0.75f, 1.0f, 1.0f);

	Sprite playerSprite(L"player.png");
	Sprite block1Sprite(L"block1.png");
	Sprite block2Sprite(L"block2.png");
	Sprite coinSprite(L"coin.png");

	playerSprite.scale = 2.0f;
	block1Sprite.scale = 2.0f;
	block2Sprite.scale = 2.0f;
	coinSprite.scale = 2.0f;

	Float3 playerSpeed;

	const int width = 168;
	const int height = 10;
	char mapData[width][height];

	std::ifstream inputFile("map1.txt");
	std::string line;

	int y = 0;
	while (std::getline(inputFile, line))
	{
		for (int x = 0; x < line.length(); x++)
		{
			mapData[x][y] = line[x];

			if (mapData[x][y] == 'P')
			{
				playerSprite.position = Float3(x * 32.0f, y * -32.0f, 0.0f);
			}
		}
		y++;
	}

	while (Refresh())
	{
		camera.position = playerSprite.position;
		camera.Update();

		if (Input::GetKey(VK_LEFT))
		{
			playerSpeed.x = -5.0f;
			playerSprite.scale.x = -2.0f;
		}

		if (Input::GetKey(VK_RIGHT))
		{
			playerSpeed.x = 5.0f;
			playerSprite.scale.x = 2.0f;
		}

		if (Input::GetKeyDown('Z'))
		{
			playerSpeed.y = 15.0f;
		}

		playerSpeed.x *= 0.9f;
		playerSpeed.y -= 1.0f;

		coinSprite.angles.y += 3.0f;

		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				Float3 tilePos = Float3(x * 32.0f, y * -32.0f, 0.0f);

				Float3 now = playerSprite.position;
				Float3 next = playerSprite.position + playerSpeed;

				switch (mapData[x][y])
				{
				case '#':
					if (isHit(tilePos, Float3(next.x, now.y, 0.0f), Float2(30.0f, 32.0f)))
					{
						if (playerSpeed.x < 0.0f)
						{
							playerSpeed.x = 0.0f;
							playerSprite.position.x = tilePos.x + 30.0f;
						}
						else if (playerSpeed.x > 0.0f)
						{
							playerSpeed.x = 0.0f;
							playerSprite.position.x = tilePos.x - 30.0f;
						}
					}
					else if (isHit(tilePos, Float3(now.x, next.y, 0.0f), Float2(30.0f, 32.0f)))
					{
						if (playerSpeed.y < 0.0f)
						{
							playerSpeed.y = 0.0f;
							playerSprite.position.y = tilePos.y + 32.0f;
						}
						else if (playerSpeed.y > 0.0f)
						{
							playerSpeed.y = 0.0f;
							playerSprite.position.y = tilePos.y - 32.0f;
						}
					}

					block1Sprite.position = tilePos;
					block1Sprite.Draw();
					break;
				case '?':
					if (isHit(tilePos, Float3(next.x, now.y, 0.0f), Float2(30.0f, 32.0f)))
					{
						if (playerSpeed.x < 0.0f)
						{
							playerSpeed.x = 0.0f;
							playerSprite.position.x = tilePos.x + 30.0f;
						}
						else if (playerSpeed.x > 0.0f)
						{
							playerSpeed.x = 0.0f;
							playerSprite.position.x = tilePos.x - 30.0f;
						}
					}
					else if (isHit(tilePos, Float3(now.x, next.y, 0.0f), Float2(30.0f, 32.0f)))
					{
						if (playerSpeed.y < 0.0f)
						{
							playerSpeed.y = 0.0f;
							playerSprite.position.y = tilePos.y + 32.0f;
						}
						else if (playerSpeed.y > 0.0f)
						{
							playerSpeed.y = 0.0f;
							playerSprite.position.y = tilePos.y - 32.0f;
							mapData[x][y] = '#';
						}
					}

					block2Sprite.position = tilePos;
					block2Sprite.Draw();
					break;
				case '0':
					if (isHit(tilePos, now, Float2(30.0f, 32.0f)))
					{
						mapData[x][y] = ' ';
					}

					coinSprite.position = tilePos;
					coinSprite.Draw();
					break;
				}
			}
		}

		playerSprite.position += playerSpeed;
		playerSprite.Draw();
	}
}
